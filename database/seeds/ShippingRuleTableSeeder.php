<?php

use Illuminate\Database\Seeder;

class ShippingRuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {//Test
        DB::table('shipping_rules')->insert(['type' => 'default',]);
        DB::table('shipping_rules')->insert(['type' => 'dont_open',]);
    }
}
