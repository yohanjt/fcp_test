<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Example of use: $this->call(UsersTableSeeder::class);
        $this->call([
            ShippingRuleTableSeeder::class,
            ProductTableSeeder::class,
            WarehouseTableSeeder::class
        ]);



    }
}
