<?php

use Illuminate\Database\Seeder;


class WarehouseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create default Warehouse with different packs quantity
        $warehouse=factory(App\Warehouse::class)->create();
        $warehouse->packs()
            ->createMany(
                factory(App\Pack::class,3)->make()->toArray()
            );


    }
}
