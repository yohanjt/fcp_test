<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * nb: Most of those column don't need to have real implementation for now
         * the "name" column will be enougth
         */

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->text('description');
            $table->json('more_infos');
            $table->float('vat',8,2)->default(0.00);
            $table->float('price',8,2);
            $table->float('total_price',8,2);
            $table->string('category')->default('default');
            $table->unsignedBigInteger('shipping_rule_id')->default('1');
            //create index
            $table->index('shipping_rule_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
