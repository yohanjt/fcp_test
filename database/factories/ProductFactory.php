<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => 'Widget',
        'description' => $faker->text,
        'more_infos' => '{}',
        'price' => 15.95,
        'total_price'=> 15.95,
        'shipping_rule_id'=>2
    ];
});
