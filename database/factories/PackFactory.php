<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pack;
use Faker\Generator as Faker;

$factory->define(Pack::class, function (Faker $faker) {

    return [
        'quantity_inside'=>$faker->randomElement([250, 500, 1000, 2000, 5000]),
        'quantity_in_stock'=>$faker->randomDigitNotNull,
        'product_id'=>1,
        'warehouse_id'=>1
    ];
});
