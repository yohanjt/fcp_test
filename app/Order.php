<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'ref_client',//customer reference
        'quantity', // quantity is the noumber of products order bythe client
        'packs_combination',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
