<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quantity_inside',
        'quantity_in_stock',
        'product_id',
        'warehouse_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * A package got only one TYPE of product and not only one product
     * like it may appear below. I'll just let it simple for now
     * @TODO review this property and use PRODUCT_TYPE instead
     */

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    /*
     *
     */
    public function warehouse()
    {
        return $this->belongsTo('App\Warehouse');
    }
}
