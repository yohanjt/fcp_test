<?php
/**
 * This interface will help us to know how to ship our package
 */
namespace App\Helpers\ShippingStrategy;

use App\Pack;
use App\Product;

Interface ShippingStrategyInterface{

    //Packet to deliver
    public function deliver(Product $product,int $quantity):array ;
}
