<?php
/**
 * Strategy to use when there is no shipping rules given.
 */

namespace App\Helpers\ShippingStrategy;

use App\Product;

class NormalShippingStrategy extends AbstractShippingStrategy
{


    public function deliver(Product $product, int $quantity): array
    {
        return [["quantity_to_ship" => $quantity, "units" => 1]];
    }
}
