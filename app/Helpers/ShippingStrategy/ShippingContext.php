<?php
/**
 * Let determine which Strategy to use
 */

namespace App\Helpers\ShippingStrategy;


use App\Product;

class ShippingContext
{

    private $shippingStrategy;

    /**
     * @TODO refactor later
     * Based on the product rule we'll chose the better strategy
     */
    public function defineStrategy(Product $product,bool $manageStock=false)
    {
        $rule = $product->shippingRule()->first();//I thikn there is better than this. @TODO refactor
        if ($rule->type === "default") {
            $this->shippingStrategy = new NormalShippingStrategy($manageStock);
        } else if ($rule->type === "dont_open") {
            $this->shippingStrategy = new DontOpenShippingStrategy($manageStock);
        }
        return $this->shippingStrategy;
    }

}
