<?php
/**
 * Strategy to use when we can't open the packs.
 *
 *
 * @TODO refactor in order to optimize
 */

namespace App\Helpers\ShippingStrategy;

use App\Product;

class DontOpenShippingStrategy extends AbstractShippingStrategy
{

    private $index = 0;

    public function deliver(Product $product, int $quantity): array
    {
        //Retrive all package in relation with the product and Sort them by their quantity
        $packs = $product->packs()->get()->sortBy('quantity_inside')->values();//->all();

        //begin calculation
        $this->setStartIndex($packs, $quantity);

        return $this->computeCombination($packs, $quantity);
    }


    /**
     * find on which index we should iterate
     * @param $packs
     * @param $quantity
     * @return int
     */
    private function setStartIndex($packs, $quantity)
    {
        $index = 0;
        while (($index < count($packs) - 1) && ($quantity > $packs[$index]->quantity_inside)) {
            $index++;
        }
        $this->index = $index;
    }


    public function stockAvailability($packs, $quantity): bool
    {
        $total_stock = 0;
        foreach ($packs as $pack) {
            $total_stock += $pack->quantity_inside * $pack->quantity_in_stock;
        }
        return ($total_stock - $quantity > 0);
    }

    //Select which method to choose weather we you the stock or no
    public function computeCombination($pack, $quantity): array
    {
        if ($this->manageStock) {
            return $this->computeCombinationWithStock($pack, $quantity);
        } else {
            return $this->computeCombinationNoStock($pack, $quantity);
        }
    }

    private function computeCombinationNoStock($packs, $quantity): array
    {
        //init variables
        $combinationResult = [];
        $node_info = (new NodeTreeInfo(0, 0, 0, 0, 0))->init();
        $nb_total_packs = 0;

        //we'll search the number of Pack who ill given our $quantity
        //since the pack collection is ordered by 'asc' we'll decrement index
        for ($index_comb=0; $this->index > -1; $this->index--) {
            $units = $packs[$this->index]->quantity_inside;
            $quantity_temp = $quantity;
            $quotient = 0;
            do {
                $quotient++;
                $quantity_temp -= $units;
            } while ($quantity_temp > 0);

            //Store information in order to compare node combinations
            $node_info->cloneIfBest(new NodeTreeInfo(
                $units,
                $quotient,
                $quantity_temp,
                ($nb_total_packs + $quotient),
                $index_comb));


            $quotient = floor($quantity / $units);
            $quantity = ($quantity % $units);
            array_push($combinationResult,
                [
                    "pack_id" => $packs[$this->index]->id, //data to send
                    "units" => $units, //data to send
                    "quantity_to_ship" => $quotient //data to send
                    , "remainder" => $quantity //to debug
                ]
            );
            //values for next loop sequence
            $nb_total_packs += $quotient;
            $index_comb++;
        }
        //If it still remains product after browse all the pack llist then Add the shortest packs
        if ($quantity > 0) {
            $quantity=(($units*$quotient) -$quantity);
            if($node_info->getRemain()<$quantity){
                $combinationResult=array_slice($combinationResult,0,($node_info->getIndex())+1);
                $combinationResult[array_key_last($combinationResult)]["quantity_to_ship"]=$node_info->getQuotient();
                $combinationResult[array_key_last($combinationResult)]["remainder"] = $node_info->getRemain();

            }else {
                $combinationResult[array_key_last($combinationResult)]["quantity_to_ship"]++;
                $combinationResult[array_key_last($combinationResult)]["remainder"] = $quantity;
                $nb_total_packs++;
            }
        }



        return array_filter($combinationResult, function ($item) {
            return $item['quantity_to_ship'] > 0;
        });
    }


    /**
     * @TODO refactor this code later, to make disappear case with nb pack= 0
     * also, if packs stock is not suffisent, return how many with be send, and how many left.
     */
    private function computeCombinationWithStock($packs, $quantity): array
    {
        //init variables
        $combinationResult = [];
        $tree_info = [];
        $nb_total_packs = 0;

        //we'll search the number of Pack who ill given our $quantity
        //since the pack collection is ordered by 'asc' we'll decrement index
        for ($index_comb = 0; $this->index > -1; $this->index--) {
            $units = $packs[$this->index]->quantity_inside;
            $quantity_temp = $quantity;
            $quotient = 0;
            do {
                $quotient++;
                $quantity_temp -= $units;
            } while ($quantity_temp > 0);

            //Store information in order to compare node combinations
            array_push($tree_info, new NodeTreeInfo(
                $units,
                $quotient,
                $quantity_temp,
                ($nb_total_packs + $quotient),
                $this->index));


            $quotient = floor($quantity / $units);
            $quantity = ($quantity % $units);
            array_push($combinationResult,
                [
                    "pack_id" => $packs[$this->index]->id, //data to send
                    "units" => $units, //data to send
                    "quantity_to_ship" => $quotient //data to send
                    , "remainder" => $quantity //to debug
                ]
            );
            //values for next loop sequence
            $nb_total_packs += $quotient;
        }
        //If it still remains product after browse all the pack llist then Add the shortest packs
        if ($quantity > 0) {
            $quantity = (($units * $quotient) - $quantity);
            $node_info = null;
            $tes=uasort($tree_info,function($node1,$node2){
                if ($node1->getRemain() == $node2->getRemain()) {
                    return 0;
                }
                return ($node1->getRemain() > $node2->getRemain()) ? -1 : 1;
            });
            foreach ($tree_info as $node_info_temp) {
                if (!($node_info_temp->getQuotient() > $packs[$node_info_temp->getIndex()]->quantity_in_stock)) {
                    $node_info = $node_info_temp;
                    break;
                }

            }

            if (isset($node_info) && $node_info->getRemain() < $quantity) {

                $combinationResult = array_slice($combinationResult, 0, (count($combinationResult)-$node_info->getIndex()));
                $combinationResult[array_key_last($combinationResult)]["quantity_to_ship"] = $node_info->getQuotient();
                $combinationResult[array_key_last($combinationResult)]["remainder"] = $node_info->getRemain();

            } else {
                $combinationResult[array_key_last($combinationResult)]["quantity_to_ship"]++;
                $combinationResult[array_key_last($combinationResult)]["remainder"] = $quantity;
                $nb_total_packs++;
            }
        }


        return array_filter($combinationResult, function ($item) {
            return $item['quantity_to_ship'] > 0;
        });
    }

}
