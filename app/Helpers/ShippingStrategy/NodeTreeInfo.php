<?php


namespace App\Helpers\ShippingStrategy;


class NodeTreeInfo
{

    private $index;
    private $quotient;
    private $unit;
    private $remain;
    private $nb_total_pack;

    /**
     * NodeTreeInfo constructor.
     *
     * @param $unit
     * @param $quotient
     * @param $remain
     * @param $nb_total_pack
     * @param $index
     */
    public function __construct($unit,$quotient,$remain,$nb_total_pack,$index )
    {
        $this->index=$index;
        $this->quotient=$quotient;
        $this->unit=$unit;
        $this->remain=$remain;
        $this->nb_total_pack=$nb_total_pack;
    }

    public function init(){
        $this->index=-1500;
        return $this;
    }

    /**
     * Select on which package should the function stop
     * by comparing:
     *  - first the remaining Products when using the pack one more time
     *  - if the remaining using a pack is the same compare the number of package send
     * return true if $this is better than $nodeTreeinfo.
     *
     *  @param NodeTreeInfo $nodeTreeInfo
     * @return bool
     *
     */
    public function compare(NodeTreeInfo $nodeTreeInfo): bool
    {
        if (abs($nodeTreeInfo->getRemain()) > abs($this->remain)) {
           return true;
        } elseif (($nodeTreeInfo->getRemain() == $this->remain) && ($nodeTreeInfo->getTotalPack() > $this->nb_total_pack)) {
            return true;
        }else{
            return false;
        }
    }

    public function getRemain()
    {
        return $this->remain;
    }

    public function getTotalPack()
    {
        return $this->nb_total_pack;
    }

    public function getIndex(){
        return $this->index;
    }

    public function getUnit(){
        return $this->unit;
    }

    public function getQuotient(){
        return $this->quotient;
    }

    /**
     * Compare thiss node and the one in the parameters
     * store the information of th last one if it better.
     * @param NodeTreeInfo $nodeTreeInfo
     */
    public function cloneIfBest(NodeTreeInfo $nodeTreeInfo){
        if(!$this->compare($nodeTreeInfo)||$this->index<0){
            $this->index = $nodeTreeInfo->getIndex();
            $this->unit = $nodeTreeInfo->getUnit();
            $this->quotient = $nodeTreeInfo->getQuotient();
            $this->remain = $nodeTreeInfo->getRemain();
            $this->nb_total_pack = $nodeTreeInfo->getTotalPack();
        }
    }
}
