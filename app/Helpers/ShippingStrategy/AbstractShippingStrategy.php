<?php


namespace App\Helpers\ShippingStrategy;


use App\Product;

abstract class AbstractShippingStrategy implements ShippingStrategyInterface
{
    protected $manageStock;

    public function __construct(bool $manageStock=false)
    {
        $this->manageStock=$manageStock;
    }


    abstract  public function deliver(Product $product, int $quantity):array ;

}
