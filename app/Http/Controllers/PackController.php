<?php

namespace App\Http\Controllers;

use App\Pack;
use App\Product;
use App\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class PackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packs = Pack::all()->sortByDesc('quantity_inside');
        return view('pack.index', ['packs' => $packs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        return view('pack.create', ["products" => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $rules = [
            'quantity_inside' => 'required|numeric|min:1',
            'quantity_in_stock' => 'required|numeric|min:0',
            'product_id' => 'required|numeric|min:1'
        ];
        Validator::make($request->all(), $rules)->validate();
        /**
         * Verify if pack doesn't already exist the same product and quantity
         * return an errors and propose to edit existing pack
         * @TODO improve by extends validator later
         */

        $nbExistingPack = DB::table('packs')
            ->where('quantity_inside', '=', $request->all()['quantity_inside'])
            ->where('product_id', '=', $request->all()['product_id'])
            ->count();
        if ($nbExistingPack > 0) {
            return back()->withErrors('This king of pack already exists. Please retrieve, this one and change it stock quantity');
        }
        /**
         * Get default Warehouse
         * @TODO refactor if multiple warehouse
         */
        $warehouse = Warehouse::find(1);
        $warehouse->packs()->create(
            $request->all()
        );
        return redirect('/warehouse');
    }

    /**
     * Show the profile for the given user.
     *
     * @param int $id
     * @return View
     */
    public function show($id)
    {
        return redirect('/warehouse');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $products = Product::all();
        $pack = Pack::find($id);
        if($pack==null){abort(404);}
        return view('pack.edit', ["products" => $products, 'pack' => $pack]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $rules = [
            'quantity_inside' => 'required|numeric|min:1',
            'quantity_in_stock' => 'required|numeric|min:0',
            'product_id' => 'required|numeric|min:1'
        ];
        Validator::make($request->all(), $rules)->validate();
        /**
         * Verify if pack doesn't already exist the same product and quantity
         * return an errors and propose to edit existing pack
         * @TODO improve by extends validator later
         */

        $existingPack = DB::table('packs')
            ->where('quantity_inside', '=', $request->all()['quantity_inside'])
            ->where('product_id', '=', $request->all()['product_id']);
        if ($existingPack->count() == 1 && $existingPack->first()->id != $id) {
            return back()->withErrors('This king of pack already exists. Please retrieve, this one and change it stock quantity');
        }
        /**
         * Get default Warehouse
         * @TODO refactor if multiple warehouse
         */
        $pack = Pack::find($id);
        $pack->quantity_inside = $request->quantity_inside;
        $pack->quantity_in_stock = $request->quantity_in_stock;
        $pack->product_id = $request->product_id;
        $pack->save();
        return redirect('/warehouse');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pack::destroy($id);
        return redirect('/warehouse');
    }
}
