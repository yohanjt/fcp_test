<?php

namespace App\Http\Controllers;

use App\Helpers\ShippingStrategy\ShippingContext;
use App\Order;
use App\Pack;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    private $strategyContext = null;

    public function __construct()
    {
        $this->strategyContext = new ShippingContext();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('order.index', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        return view('order.create', ["products" => $products]);
    }

    /**
     * Visualize data without save it first
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function visualization(Request $request)
    {
        $rules = [
            'quantity' => 'required|numeric|min:1',
            'ref_client' => 'required',
            'product_id' => 'required|numeric|min:1'
        ];
        $manage_stock = isset($request->manage_stock);
        Validator::make($request->all(), $rules)->validate();
        $pack_combination_array = $this->getCombination(Product::find($request->product_id), $request->quantity,$manage_stock);
         $total_product_shipped = array_reduce($pack_combination_array, function ($total, $item) {
            return $total += $item['quantity_to_ship'] * $item['units'];
        });


        return view('order.show',
            [
                "combinations" => $pack_combination_array,
                "quantity" => $request->quantity,
                "product" => Product::find($request->product_id),
                "ref_customer" => $request->ref_client,
                'total_product_shipped'=>$total_product_shipped,
                'remained'=>($total_product_shipped-$request->quantity)
            ]);
    }

    /**
     * Show the profile for the given user.
     *
     * @param int $id
     * @return View
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        return view('order.show', ['order' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $products = Product::all();
        $order = Order::find($id);
        return view('order.edit', ["products" => $products, 'order' => $order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $rules = [
            'quantity_inside' => 'unique',
            'quantity_in_stock' => 'required|numeric',
            'product_id' => 'required|numeric'
        ];
        $validator = Validator::make($request->all(), $rules);
        /**
         * Verify if pack doesn't already exist the same product and quantity
         * return an errors and propose to edit existing pack
         * @TODO improve by extends validator later
         */

        $nbExistingPack = DB::table('packs')
            ->where('quantity_inside', '=', $request->all()['quantity_inside'])
            ->where('product_id', '=', $request->all()['product_id'])
            ->count();
        if ($nbExistingPack > 0) {
            return back()->withErrors('This king of pack already exists. Please retrieve, this one and change it stock quantity');
        }
        /**
         * Get default Warehouse
         * @TODO refactor if multiple warehouse
         */
        $pack = Pack::find($id);
        $pack->quantity_inside = $request->quantity_inside;
        $pack->quantity_in_stock = $request->quantity_in_stock;
        $pack->product_id = $request->product_id;
        $pack->save();
        return redirect('/orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::destroy($id);
        return redirect('/orders');
    }

    /*
     * This will give us how to send our packs
     */
    private function getCombination(Product $product, int $quantity,$manage_stock=false)
    {
        //Retrieve with method to select packs
        $shippingMethod = $this->strategyContext->defineStrategy($product,$manage_stock);// this second parameter will say if should use packs stock or no
        return $shippingMethod->deliver($product, $quantity);//compute pack selection
    }
}
