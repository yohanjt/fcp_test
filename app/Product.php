<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'more_infos',
        'vat',
        'price',
        'total_price',
        'category',
        'shipping_rule_id'
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'category' => 'default',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * The differents packs a product can be shipped by
     */
    public function packs(){
        return $this->hasMany('App\Pack');
    }

    public function shippingRule()
    {
        return $this->belongsTo('App\ShippingRule');
    }

    /**
     * @param $value
     * Total price should be only the result from price and V.A.T
     */
    public function getTotalPriceAttribute(){
       return $this->attributes['price']+$this->attributes['vat'];
    }

}
