<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected  $fillable=[
        'name',
        'postcode'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * A Warehouses possess different packs of differents products
     */
    public function packs(){
        return $this->hasMany('App\Pack');
    }
}
