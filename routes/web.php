<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PackController@index');


/**----------------------------------------------------------------------------------------
 * It would maybe confusing cause I don't use the name of the ressource Model
 * but just because I prefer see "warehouse" in the url than "packs" or "package".
 *
 *                          WAREHOUSE
 */

Route::get('/warehouse', 'PackController@index');//List all packages (or packs) in the Warehouse
Route::post('/warehouse', 'PackController@store');//Create new package
Route::get('/warehouse/create', 'PackController@create'); //Propose to create new package
Route::get('/warehouse/{package}', 'PackController@show');//View a package in a signle view
Route::get('/warehouse/{package}/edit', 'PackController@edit');//modify the current Package
Route::put('/warehouse/{package}', 'PackController@update');//save modification of the package
Route::delete('/warehouse/{package}', 'PackController@destroy'); //destroy the package

/** -----------------------------------------------------------------------------------------
 *                          Orders
 */
Route::get('/orders', 'OrderController@index');
Route::get('/orders/create', 'OrderController@create');
Route::get('/orders/{order_id}', 'OrderController@show');
Route::post('/orders', 'OrderController@visualization');
//Route::post('/orders', 'OrderController@store');

