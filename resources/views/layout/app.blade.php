<!-- Stored in resources/views/layouts/app.blade.php -->

<html>
<head>
    <title>FCP test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, user-scalable=no">
</head>
<body>
<nav class="navbar navbar-expand-md navbar-light navbar-laravel border-bottom mb-5 ">
    <div class="container">

        <span class="navbar-brand">FCP Challenge ></span>

        <div>@yield('breadcrump')</div>


        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="ml-auto">
                @yield('actions')
            </div>
        </div>
    </div>
    <hr>
</nav>

<div class="container">
    @yield('content')
</div>
<div class="navbar fixed-bottom bg-info ">
    <div class="mx-auto">
        <a href="/orders" class="text-white mr-4">Orders</a>
        <a href="/warehouse" class="active text-white">Warehouse</a>
    </div>
</div>
</body>
</html>
