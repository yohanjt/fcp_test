@extends('layout.app')

@section('breadcrump')
    <span class="h4 align-items-baseline">Warehouse Packs</span>
    <span class="mr-3 ml-3">></span>
    <span class="h4 align-items-baseline"> Pack modification</span>
@endsection

@section('actions')
    <ul class="navbar-nav">
        <li class="nav-item mr-1">
            <a href="/warehouse" class="btn btn-info btn-sm">cancel</a>
        </li>
        <li class="nav-item ">
            <form method="post" action="/warehouse/{{$pack->id}}">
                @csrf
                @method('DELETE')
                <button class="btn btn-info btn-sm">delete</button>
            </form>
        </li>
    </ul>
@endsection


@section('content')
    <div class="row mb-2">
        <div class="col-12">
            <p>
            @foreach($errors->all() as $message)
                    <span><strong class="text-danger ">{{$message}}</strong></span> <br>
            @endforeach
            </p>
        </div>
    </div>

    <div class="row">
        <div class="card  p-3 border-info col 12">
            <form method="post" action="/warehouse/{{$pack->id}}">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label for="product_selc">Which product your pack contains</label>
                    <select class="form-control" name="product_id" id="product_selc">
                        @foreach($products as $product)
                            <option value="{{$product->id}}">{{$product->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="quantity_ins_form">Write the exact number of products this pack should
                            contains</label>
                        <input type="number" name="quantity_inside" class="form-control" id="quantity_ins_form"
                               value="{{$pack->quantity_inside}}"
                               placeholder="ex: 250" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="quantity_stock_form">How many pack of this type are in Stock</label>
                        <input type="number" class="form-control" name="quantity_in_stock" id="quantity_stock_form"
                               required
                               value="{{$pack->quantity_in_stock}}"
                               placeholder="Quantity in stock. Ex: 90">
                    </div>
                </div>


                <button type="submit" class="btn btn-primary">Modify</button>
            </form>
        </div>
    </div>
@endsection

