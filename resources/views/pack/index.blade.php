@extends('layout.app')

@section('breadcrump')
    <span class="h4 align-items-baseline">Warehouse Packs</span>
    <span class="mr-3 ml-3">></span>
    <span class="h4 align-items-baseline">all Packs</span>
@endsection
@section('actions')
    <ul class="navbar-nav">

        <li class="nav-item">
            <a href="/warehouse/create" class="btn btn-info btn-sm">Add a new Pack</a>
        </li>
    </ul>
@endsection
@section('content')

    {{--@TODO display message after creation/destroy/update when redirect--}}
    <div class="row mb-5">
        <div class="table table-hover table-dark p-3 rounded">
            <div class="row">
                <div class="col"><h6><strong>Product</strong></h6></div>
                <div class="col"><h6><strong>Units per Pack</strong></h6></div>
                <div class="col"><h6><strong>in Stock</strong></h6></div>
            </div>
            <hr class="border-info">
            {{--@TODO display something to show there is no data yet. propose to create if no existe--}}
            @foreach ($packs as $pack)
                <div class="row">
                    <a href="/warehouse/{{$pack->id}}/edit" class="text-white col-12">

                        <div class="row ">
                            <div class="col ">{{$pack->product->name}}</div>
                            <div class="col">{{$pack->quantity_inside}}</div>
                            <div class="col">{{$pack->quantity_in_stock}}</div>
                        </div>
                        <hr>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
