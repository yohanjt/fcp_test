@extends('layout.app')

@section('breadcrump')
    <span class="h4 align-items-baseline">Shipping Orders</span>
    <span class="mr-3 ml-3">></span>
    <span class="h4 align-items-baseline">Orders creation</span>
@endsection

@section('actions')
    <ul class="navbar-nav">
        <li class="nav-item ">
            <a href="/warehouse" class="btn btn-info btn-sm">cancel</a>
        </li>
    </ul>
@endsection

@section('content')
    <div class="row mb-2">
        <div class="col-12">
            <p>
                @foreach($errors->all() as $message)
                    <span><strong class="text-danger ">{{$message}}</strong></span> <br>
                @endforeach
            </p>
        </div>
    </div>
    <div class="row">
        <div class="card  p-3 border-info col-12">
            <form method="POST" action="/orders">
                @csrf

                <div class="form-group">
                    <label for="product_selc">The Ordered Product </label>
                    <select class="form-control" name="product_id" id="product_selc">
                        @foreach($products as $product)
                            <option value="{{$product->id}}">{{$product->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="quantity_ins_form">Quantity ordered</label>
                        <input type="number" name="quantity" class="form-control" id="quantity_ins_form"
                               placeholder="250" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ref_client_form">Customer reference</label>
                        <input type="text" class="form-control" name="ref_client" id="ref_client_form" value="#1092202dv3"
                               required
                               placeholder="ex: #1092202dv3">
                    </div>
                </div>

                <div class="form-check mt-1 mb-5">
                    <input class="form-check-input" type="checkbox" name="manage_stock" id="manage_stock">
                    <label class="form-check-label" for="manage_stock">
                        Use stock quantity
                    </label>
                </div>


                <button type="submit" class="btn btn-primary">Visualize</button>
            </form>
        </div>
    </div>

@endsection
