@extends('layout.app')

@section('breadcrump')
    <span class="h4 align-items-baseline">Shipping Orders</span>
    <span class="mr-3 ml-3">></span>
    <span class="h4 align-items-baseline">Visualization</span>
@endsection

@section('actions')
    <ul class="navbar-nav">
        <li class="nav-item ">
            <a href="/orders" class="btn btn-info btn-sm">cancel</a>
        </li>
    </ul>
@endsection

@section('content')
    <div class="row mb-2">
        <div class="col-12">
            <p>
                @foreach($errors->all() as $message)
                    <span><strong class="text-danger ">{{$message}}</strong></span> <br>
                @endforeach
            </p>
        </div>
    </div>
    <div class="row mb-5">
        <div class="card  p-3 border-info col-12">
            <div class="h5 row p-2">
                <p>
                    <span>The customer {{$ref_customer}} ordered <strong class="text-info">{{$quantity}}</strong> of {{$product->name}}s.</span>
                    <br>
                    <span>You will have to send him {{$product->name}}  packs this way:</span>
                </p>
            </div>
            <div class="row p-3">
                <ul>
                    @foreach($combinations as $combination)
                        <li>
                            <span class="font-weight-bold ml-3">{{$combination['quantity_to_ship']}}</span>
                            <span class="mr-1">x</span>
                            <span class="font-weight-bold mr-1"> {{$combination['units']}}</span><span>units</span>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="row p-3">
                <p>
                    <span>
                    You will deliver <strong class="">{{$total_product_shipped}}</strong> {{$product->name}}
                     a.k.a  <strong>{{abs($remained)}}</strong>
                    @if($remained>=0)
                    more products than ordered.
                    @else
                        products to ship later (packs in stock are a bit short).
                    @endif
                    </span>
                </p>

            </div>


        </div>
    </div>

@endsection
