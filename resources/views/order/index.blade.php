@extends('layout.app')

{{--@TODO refactorate breadcrumbs later--}}
@section('breadcrump')
    <span class="h4 align-items-baseline">Shipping Orders</span>
    <span class="mr-3 ml-3">></span>
    <span class="h4 align-items-baseline">all orders</span>
@endsection
@section('actions')
    <ul class="navbar-nav">

        <li class="nav-item">
            <a href="/orders/create" class="btn btn-info btn-sm">Add a new order</a>
        </li>
    </ul>
@endsection
@section('content')


    <div class="table table-hover table-dark p-3 rounded">
        <div class="row">
            <div class="col"><h6><strong>ref custom</strong></h6></div>
            <div class="col"><h6><strong>product</strong></h6></div>
            <div class="col"><h6><strong>quantity</strong></h6></div>
            <div class="col"><h6><strong>date</strong></h6></div>
            <div class="col"><h6><strong> Qty to Ship</strong></h6></div>
        </div>
        <hr class="border-info">
        {{--@TODO display something to show there is no data yet. propose to create if no existe--}}
        @foreach ($orders as $order)
            <div class="row">
                <a href="/orders/{{$order->id}}/edit" class="text-info">
                    <hr>
                    <div class="row ">
                        <div class="col ">{{$order->ref_client}}</div>
                        <div class="col ">{{$order->product->name}}</div>
                        <div class="col">{{$order->quantity}}</div>
                        <div class="col">{{$order->quantity}}</div>
                        <div class="col">{{$order->total}}</div>
                    </div>
                </a>
            </div>
        @endforeach
        <div class="row mt-5">
            <div class="col-12 mt-5 mb-5 d-flex justify-content-center">
                <p class="text-center">
                    <span class="h4">Sorry orders won't be stored for now.</span>
                    <br>
                    <span class="h4">Luckily, you'll be able to simulate those</span>
                    <br>
                    <a href="/orders/create" class="btn btn-info mt-5">Try Here</a>
                </p>
            </div>
        </div>
    </div>


@endsection
