@extends('layout.app')

@section('breadcrump')
    <span class="h4 align-items-baseline">Orders</span>
    <span class="mr-3 ml-3">></span>
    <span class="h4 align-items-baseline">all orders</span>
@endsection
@section('actions')
    <ul class="navbar-nav">

        <li class="nav-item">
            <a href="/order/create" class="btn btn-info btn-sm">Add a new order</a>
        </li>
    </ul>
@endsection
@section('content')


    <div class="table table-hover table-dark p-3 rounded">
        <div class="row">
            <div class="col"><h6><strong>Product</strong></h6></div>
            <div class="col"><h6><strong>Units per order</strong></h6></div>
            <div class="col"><h6><strong>in Stock</strong></h6></div>
        </div>

        @foreach ($orders as $order)

            <a href="/order/{{$order->id}}/edit" class="text-info">
                <hr>
                <div class="row">
                    <div class="col ">{{$order->product->name}}</div>
                    <div class="col">{{$order->quantity_inside}}</div>
                    <div class="col">{{$order->quantity_in_stock}}</div>
                </div>
            </a>
        @endforeach
    </div>


@endsection
